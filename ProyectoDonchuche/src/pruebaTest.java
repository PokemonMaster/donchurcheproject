import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class pruebaTest {

	@Test
	void test1() {
		
		int k = 4;
		int t = 8;
		int p = 24;
		int[] linia = HonorBambino.linia2(k);
		
		
		assertEquals("1", HonorBambino.calcular(k, t, p, linia));
		
		k = 3;
		t = 4;
		p = 23;
		linia = HonorBambino.linia2(k);
		
		assertEquals("HONOR BAMBINO", HonorBambino.calcular(k, t, p, linia));
		
		k = 3;
		t = 4;
		p = 23;
		linia = HonorBambino.linia2(k);
		
		assertEquals("HONOR SIR ENGINEER", HonorBambino.calcular(k, t, p, linia));		
		k = 1;
		t = 4;
		p = 10;
		linia = HonorBambino.linia2(k);
		
		assertEquals("HONOR BAMBINO", HonorBambino.calcular(k, t, p, linia));
		
		k = 1;
		t = 0;
		p = 15;
		linia = HonorBambino.linia2(k);
		assertEquals("HONOR BAMBINO", HonorBambino.calcular(k, t, p, linia));

	}

}

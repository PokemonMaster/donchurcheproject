import java.util.ArrayList;
import java.util.Scanner;

public class HonorBambino {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        int ncas = sc.nextInt();
        sc.nextLine();
        for (int i = 0; i < ncas; i++) {
            int k = sc.nextInt();
            int t = sc.nextInt();
            int p = sc.nextInt();
            sc.nextLine();
            int[] linia = linia2(k);
            String coso = calcular(k, p, t, linia);
            System.out.println(coso);
        }

    }

    public static String calcular(int k, int p, int t, int[] linia) {
        // TODO Auto-generated method stub
        ArrayList<Integer> algo = tiempo(t, p);
        int cont = 0;
        for (int i = 0; i < linia.length; i++) {
            if (algo.contains(linia[i])) {
                cont++;
            }
        }
        if (cont == k) {
            return "HONOR BAMBINO";
        } else if (cont == 0) {
            return "HONOR SIR ENGINEER";
        } else {
            String holi = ""+cont;
            return holi;
        }
    }

    private static ArrayList<Integer> tiempo(int t, int p) {
        // TODO Auto-generated method stub
        ArrayList<Integer> algo = new ArrayList<Integer>();
        for (int i = t; i <= p; i += (t + 3)) {
            algo.add(i);
            algo.add(i + 1);
            algo.add(i + 2);
        }
        return algo;
    }

    public static int[] linia2(int k) {
        // TODO Auto-generated method stub
        int[] algo = new int[k];
        for (int i = 0; i < k; i++) {
            int num = sc.nextInt();
            algo[i] = num;
        }
        return algo;
    }

}